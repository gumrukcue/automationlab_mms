#include "iec61850_client.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

int
main(int argc, char** argv)
{

    char* hostname;
    int tcpPort = 104; /*Server adress*/

    if (argc > 1)
        hostname = argv[1];
    else
        hostname = "localhost";

    if (argc > 2)
        tcpPort = atoi(argv[2]);

    IedClientError error;
    MmsError mss_error;

	IedConnection myClient2 = IedConnection_create();
    IedConnection_connect(myClient2, &error, hostname, tcpPort);

    if (error == IED_ERROR_OK) {

        printf("Get logical device list...\n");
        LinkedList deviceList = IedConnection_getLogicalDeviceList(myClient2, &error);
        LinkedList device = LinkedList_getNext(deviceList);

        while (device != NULL) {
            printf("LD: %s\n", (char*) device->data);
			
            //START TASK 3.2
            //Declare a LinkedList
            //Hint use function 'IedConnection_getLogicalDeviceVariables'
            
            
            //
            //A While loop through logical device variables to print them
            
            
            
            
            //End of while loop
            
            //END TASK 3.2

            device = LinkedList_getNext(device);
        }
		//End While loop for logical nodes

        //START TASK 4.2
        //Read MMS value for the updated analog measurement by client request
        //Declare a MmsConnection struct and assign the MMS connection of myClient2
        //Hint: Use function 'IedConnection_getMmsConnection'
        
        //
        //Read MMS value of the data attribute which was updated in the server 
        //Hint: Use 'MmsConnection_readVariable' function
        
        //
        //Declare a float to convert the mms value to float 
        //Hint: Use 'MmsValue_toFloat'
        
        //
        //Print float value
        
        //
        //END TASK 4.2

        LinkedList_destroy(deviceList);
        IedConnection_close(myClient2);
    }
    else {
        printf("Connection failed!\n");
    }
   

cleanup_and_exit:
    IedConnection_destroy(myClient2);
}
