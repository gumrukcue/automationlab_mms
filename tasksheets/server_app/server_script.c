#include "iec61850_server.h"
#include "hal_thread.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "static_model.h"

/* import IEC 61850 device model created from SCL-File */
extern IedModel iedModel;
static int running = 0;

static void
connectionHandler (IedServer self, ClientConnection connection, bool connected, void* parameter)
{
	// START TASK 1.1
	// If connected==True print 'connected'


	// Else print 'connection closed'


	// END TASK 1.1
}

int main(int argc, char** argv) {

    int tcpPort = 104;

    if (argc > 1) {
        tcpPort = atoi(argv[1]);
    }

	// START TASK 1.2
	// Create an IedServer object and name it as myServer
	// Hint: Use IedServer_create function

	// END TASK 1.2
	
	// START TASK 1.3
	// Add the connection handler written in step 1.1 to myServer
	// Hint: Use 'IedServer_setConnectionIndicationHandler' function
	
	// END TASK 1.3
	
	// START TASK 1.4
	// Instruct myServer to listen to the connections
	// Hint: Use 'IedServer_start' function






	// END TASK 1.4
	
	running = 1;
	while (running) {
		IedServer_lockDataModel(myServer);
		IedServer_unlockDataModel(myServer);
		Thread_sleep(1000);

		/* This block is a hint for TASK 4.1. 
		float energy=200.f;
		MmsValue* energyValue = MmsValue_newFloat(energy);
		IedServer_updateAttributeValue(myServer,IEDMODEL_Device1_DGEN1_TotWh_mag_f,energyValue);
		*/

		// START TASK 4.1
		// Update the value of the attribute IEDMODEL_Device1_MMXU2_TotW_mag_f
		// Declare a float, assign 100.f
		
		//
		// Convert this float to float
		// Hint use MmsValue_newFloat
		
		//
		//Update the attribute value
		//Hint: Use 'IedServer_updateAttributeValue' function
		
		//
		// END TASK 4.1
	}

	 
	IedServer_stop(myServer); //stop MMS server - close TCP server socket and all client sockets 
	IedServer_destroy(myServer); // Cleanup - free all resources
	
} 
