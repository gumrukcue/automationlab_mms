#include "iec61850_client.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

int
main(int argc, char** argv)
{

    char* hostname;
    int tcpPort = 104; /*Server adress*/

    if (argc > 1)
        hostname = argv[1];
    else
        hostname = "localhost";

    if (argc > 2)
        tcpPort = atoi(argv[2]);

    IedClientError error;
    MmsError mss_error;

    //START TASK 1.5
	// Create an IedConnection object and name it as myClient
	// Hint: Use 'IedConnection_create' function 
	
	//END TASK 1.5
	
	//START TASK 1.6
	// Connect myClient to the server in the specified port
	// Hint: Use 'IedConnection_connect' function 
    
	//END TASK 1.6
	
	
	//START TASK 2.3
	// Obtain the tree shown in Figure 7
	// Hint use LinkedList structure to loop through LD, LN, DO
    if (error == IED_ERROR_OK) {
	/*Comment out this line for TASK 2.3
	
        // This block is a hint for TASK 2.3
        printf("Get logical device list...\n");
        LinkedList deviceList = IedConnection_getLogicalDeviceList(myClient, &error);
        LinkedList device = LinkedList_getNext(deviceList);
        
        //A While loop through logical devices
        
            //Print logical device name
            
			//Declare a LinkedList struct for the logical nodes of this logical device and name it locigalNodes
            
            
			//
			//A While loop through logical nodes
			
                //Print logical node name
                
				//
				//Declare a string called 'logicalnoderef' and save the logical node reference in it: logicalnoderef=LDName/LNName 
				
                
				//
				//Get the directory of logicalnode containing elements of ACSI_CLASS_DATA_OBJECT, assign it to a LinkedList called data objects
				
                
				//
				//A While loop through data objects
                
                    //Print data object name
                    
                    //             
                    //Next data object 
                    
                    //
                
                //End While loop for data objects
                // Destroy the linked list dataObjects
                
				//

				//Get next logical node
                
				//
			
			//End While loop for logical nodes
            //Destroy logicalNodes
            
            //
            //Get next logical device
            
            //
        
		//End While loop for logical devices
        
        //Destroy logical devices
        LinkedList_destroy(deviceList);
        //
		
	Comment out this line for TASK 2.3*/
        IedConnection_close(myClient);
        
    }
    else {
        printf("Connection failed!\n");
    }
   

cleanup_and_exit:
    IedConnection_destroy(myClient);
}